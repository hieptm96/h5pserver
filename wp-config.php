<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'h5pserver');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'hiep12');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'dnf_:;|E5H%y![A:l3=^Ygx5(COBFZ1EZK[$!38N1rQET9b^M9x.16NH=d0+MH0!');
define('SECURE_AUTH_KEY',  'fm _G`/=g$q1R,d);c[h(>P)N27cW,0]_3=hi_,;WKDl_z5eLq~t32Ian`.dV.r ');
define('LOGGED_IN_KEY',    ')%l*=nmG$GYFqQ*3OHH<I(+ ]jVG*[OSA*1LAOFV,uHs7}/E|0_U_}(]yQFi>Mg>');
define('NONCE_KEY',        'dx<m,lrN*4&n<ZjBxK$Emjs<f,.?rN=H5ay2dsJowffg(d:_Jic#EZ*tO_eEX=/=');
define('AUTH_SALT',        'Ty&xf/x@= 1}_KTf${W%*^T4p(oj.vpM<|4B:,wr[=S4TK@!lUOB.0G<#6%e%UiJ');
define('SECURE_AUTH_SALT', '8VJuU0!Mv+P8F,mWBP8K0gAp9#@^Gr=2%*N0%Y|R6cI#:;zq,6f*0x+<,jbws6hQ');
define('LOGGED_IN_SALT',   'MvJA8!X?-n1%-DX?>`ua6r8kL.LhV5mb^iBq9o,`|lt@|)&yX,^+R@,ot;Q0SXAY');
define('NONCE_SALT',       'oIRCL81]Wq:MLJ5d,w&@EKGyE-PyCVGxO}a V8_n&fN=L~vlb:_.3q)YCjF5g|_K');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
// define('WP_DEBUG', false);
define('WP_DEBUG', true);
define( 'WP_DEBUG_LOG', true );
define('FS_METHOD', 'direct');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
